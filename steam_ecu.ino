#define CRANK_RESET_INTERRUPT_PIN PD2
#define CRANK_RESET_POSITION 0

#define ENCODER_INTERRUPT_PIN_A PD3
#define ENCODER_PIN_B PD4
#define ENCODER_RESOLUTION 200

#define INTAKE_VALVE_A_PIN PD5
#define INTAKE_VALVE_B_PIN PD6
#define EXHAUST_VALVE_A_PIN PD7
#define EXHAUST_VALVE_B_PIN PB0

#define INTAKE_OPEN_PERCENT_PIN A0
#define INTAKE_OPEN_PERCENT_ADC_RESOLUTION 1023

volatile int crank_position = 0;

volatile float crank_position_percent = 0.0;
volatile float intake_open_percent = 0.5;

volatile unsigned long last_encoder_interrupt_time;
volatile unsigned long current_encoder_interrupt_time;
volatile int current_revolutions_per_minute = 0;

int getRevolutionsPerMinute(unsigned long interrupt_time_delta_us, int interrupts_per_revolution) {
  return 1 / (interrupt_time_delta_us * interrupts_per_revolution / 1000000) * 60;
}

int getCrankPositionIncrement(uint8_t encoder_pin_a, uint8_t encoder_pin_b) {
  if (encoder_pin_b == 1) {
    return 1;
  }

  return -1;
}

int getDesiredIntakeValveState_A(float crank_position_percent, float intake_open_percent) {
  if (crank_position_percent < 0.5 && crank_position_percent / 0.5 < intake_open_percent) {
    return 1;
  }

  return 0;
}

int getDesiredIntakeValveState_B(float crank_position_percent, float intake_open_percent) {
  if (crank_position_percent > 0.5 && (crank_position_percent / 0.5) - 1 < intake_open_percent) {
    return 1;
  }

  return 0;
}

int getDesiredExhaustValveState_A(float crank_position_percent) {
  if (crank_position_percent > 0.5) {
    return 1;
  }

  return 0;
}

int getDesiredExhaustValveState_B(float crank_position_percent) {
  if (crank_position_percent < 0.5) {
    return 1;
  }

  return 0;
}

void setup() {
  Serial.begin(9600);
  Serial.println(getRevolutionsPerMinute(5000, 200) == 60 ? "OK" : "ERROR: expected getRevolutionsPerMinute to return 60");
  Serial.println(getRevolutionsPerMinute(1000, 200) == 300 ? "OK" : "ERROR: expected getRevolutionsPerMinute to return 300");
  Serial.println(getCrankPositionIncrement(1, 1) == 1 ? "OK" : "ERROR: expected getCrankPositionIncrement to return 1");
  Serial.println(getCrankPositionIncrement(1, 0) == -1 ? "OK" : "ERROR: expected getCrankPositionIncrement to return -1");
  Serial.println(getDesiredIntakeValveState_A(0.2, 0.5) == 1 ? "OK" : "ERROR: expected getDesiredIntakeValveState_A to return 1");
  Serial.println(getDesiredIntakeValveState_A(0.3, 0.5) == 0 ? "OK" : "ERROR: expected getDesiredIntakeValveState_A to return 0");
  Serial.println(getDesiredIntakeValveState_B(0.7, 0.5) == 1 ? "OK" : "ERROR: expected getDesiredIntakeValveState_B to return 1");
  Serial.println(getDesiredIntakeValveState_B(0.8, 0.5) == 0 ? "OK" : "ERROR: expected getDesiredIntakeValveState_B to return 0");
  Serial.println(getDesiredExhaustValveState_A(0.6) == 1 ? "OK" : "ERROR: expected getDesiredExhaustValveState_A to return 1");
  Serial.println(getDesiredExhaustValveState_A(0.4) == 0 ? "OK" : "ERROR: expected getDesiredExhaustValveState_A to return 0");
  Serial.println(getDesiredExhaustValveState_B(0.4) == 1 ? "OK" : "ERROR: expected getDesiredExhaustValveState_B to return 1");
  Serial.println(getDesiredExhaustValveState_B(0.6) == 0 ? "OK" : "ERROR: expected getDesiredExhaustValveState_B to return 0");

  pinMode(CRANK_RESET_INTERRUPT_PIN, INPUT);
  pinMode(ENCODER_INTERRUPT_PIN_A, INPUT);
  pinMode(ENCODER_PIN_B, INPUT);
  pinMode(INTAKE_OPEN_PERCENT_PIN, INPUT);

  pinMode(INTAKE_VALVE_A_PIN, OUTPUT);
  pinMode(INTAKE_VALVE_B_PIN, OUTPUT);
  pinMode(EXHAUST_VALVE_A_PIN, OUTPUT);
  pinMode(EXHAUST_VALVE_B_PIN, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(CRANK_RESET_INTERRUPT_PIN), ISR_resetCrankPosition, RISING);
  attachInterrupt(digitalPinToInterrupt(ENCODER_INTERRUPT_PIN_A), ISR_updateCrankPosition, RISING);
}

void loop() {
  intake_open_percent = analogRead(INTAKE_OPEN_PERCENT_PIN) / INTAKE_OPEN_PERCENT_ADC_RESOLUTION;
  
  Serial.print("IOP: ");
  Serial.print(floor(intake_open_percent * 100));

  Serial.print(", RPM: ");
  Serial.print(current_revolutions_per_minute);
  Serial.print('\n');

  delay(500);
}

void updateRevolutionsPerMinute() {
  current_encoder_interrupt_time = micros();
  current_revolutions_per_minute = getRevolutionsPerMinute((current_encoder_interrupt_time - last_encoder_interrupt_time), ENCODER_RESOLUTION);
  last_encoder_interrupt_time = current_encoder_interrupt_time;
}

void ISR_resetCrankPosition() {
  crank_position = CRANK_RESET_POSITION;
}

void ISR_updateCrankPosition() {
  updateRevolutionsPerMinute();

  crank_position += getCrankPositionIncrement(1, digitalRead(ENCODER_PIN_B));

  if (crank_position > ENCODER_RESOLUTION) {
    ISR_resetCrankPosition();
  }

  crank_position_percent = crank_position / ENCODER_RESOLUTION;

  digitalWrite(INTAKE_VALVE_A_PIN, getDesiredIntakeValveState_A(crank_position_percent, intake_open_percent));
  digitalWrite(INTAKE_VALVE_B_PIN, getDesiredIntakeValveState_B(crank_position_percent, intake_open_percent));
  digitalWrite(EXHAUST_VALVE_A_PIN, getDesiredExhaustValveState_A(crank_position_percent));
  digitalWrite(EXHAUST_VALVE_B_PIN, getDesiredExhaustValveState_B(crank_position_percent));
}
